package com.example.morejump.contactapp;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by morejump on 09/11/2017.
 */

public class PersonAdapter extends BaseAdapter {
    ArrayList<Person> listData;
    LayoutInflater inflater;
    TextView txtName, placeholder;
    ImageView imgAvatar, imgCheck;
    public PersonAdapter(ArrayList<Person> listData, Context context) {
        this.listData = listData;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public ArrayList<Person> getListData() {
        return listData;
    }

    public void setListData(ArrayList<Person> listData) {
        this.listData = listData;
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Person getItem(int i)
    {
        return listData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        if (convertView == null){
            convertView = inflater.inflate(R.layout.custome_item, null);
        }
        txtName = convertView.findViewById(R.id.txtName);
        placeholder = convertView.findViewById(R.id.placeholder);
        imgAvatar = convertView.findViewById(R.id.imgAvatar);
        imgCheck = convertView.findViewById(R.id.imgCheck);
        Person person = listData.get(i);
        txtName.setText(person.getName().toString());
        if (!person.isChecked()){
            if (person.getAvatar() == null){
                placeholder.setText(getShortName(person.getName().toString()));
                int[] androidColors = convertView.getResources().getIntArray(R.array.androidcolors);
                int randomAndroidColor = androidColors[new Random().nextInt(androidColors.length)];
                GradientDrawable background = new GradientDrawable();
                background.setCornerRadius(1000);
                background.setColor(randomAndroidColor);
                placeholder.setBackground(background);
                imgCheck.setVisibility(View.INVISIBLE);
                placeholder.setVisibility(View.VISIBLE);
            } else {
                Glide.with(imgAvatar.getContext()).load(person.getAvatar()).apply(RequestOptions.circleCropTransform()).into(imgAvatar);
                placeholder.setVisibility(View.INVISIBLE);
                imgCheck.setVisibility(View.INVISIBLE);
                imgAvatar.setVisibility(View.VISIBLE);
//            imgAvatar.setImageBitmap(BitmapFactory.decodeFile(person.getAvatar()));
            }
        } else {
            placeholder.setVisibility(View.INVISIBLE);
            imgAvatar.setVisibility(View.INVISIBLE);
            imgCheck.setVisibility(View.VISIBLE);
        }
        return convertView;
    }

    public String getShortName(String name) {
        String[] strings = name.split(" ");//no i18n
        String shortName;
        if (strings.length == 1) {
            shortName = strings[0].substring(0, 1);
        } else {
            shortName = strings[0].substring(0, 1);
        }
        return shortName.toUpperCase();
    }

}
