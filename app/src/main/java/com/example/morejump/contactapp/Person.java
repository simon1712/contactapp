package com.example.morejump.contactapp;

import java.io.Serializable;

/**
 * Created by morejump on 09/11/2017.
 */

public class Person implements Serializable {
    private int id;
    private String name;
    private String phone;
    private String email;
    private String avatar;
    private boolean isChecked;

    public Person() {

    }

    public Person(int id, String name, String phone, String email, String avatar) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.avatar = avatar;
    }

    public Person(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
