package com.example.morejump.contactapp;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.Random;

public class DetailActivity extends AppCompatActivity {

    TextView txtDetailName, txtDetailPhone, txtDetailMail, txtDetailPlaceHolder;
    ImageView imgDetailAvatar, imgSMS, imgCall, imgEmail;
    Person person;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DetailActivity.this, EditActivity.class);
                intent.putExtra("EDIT", person);
                startActivityForResult(intent, 20);
            }
        });
        initView();
    }

    public void initView() {
        txtDetailName = (TextView) findViewById(R.id.txtDetailName);
        txtDetailPhone = (TextView) findViewById(R.id.txtDetailPhone);
        txtDetailMail = (TextView) findViewById(R.id.txtDetailMail);
        txtDetailPlaceHolder = (TextView) findViewById(R.id.txtDetailPlaceHolder);
        imgDetailAvatar = (ImageView) findViewById(R.id.imgDetailAvatar);
        imgSMS = findViewById(R.id.imgSMS);
        imgCall = findViewById(R.id.imgCall);
        imgEmail = findViewById(R.id.imgEmail);


        if (getIntent().getExtras() != null) {
            person = (Person) getIntent().getSerializableExtra("DETAIL");
        txtDetailName.setText(person.getName());
        txtDetailPhone.setText(person.getPhone());
        txtDetailMail.setText(person.getEmail());

        if (person.getAvatar() == null) {
            txtDetailPlaceHolder.setText(getShortName(person.getName().toString()));
            int[] androidColors = getResources().getIntArray(R.array.androidcolors);
            int randomAndroidColor = androidColors[new Random().nextInt(androidColors.length)];
            GradientDrawable background = new GradientDrawable();
            background.setCornerRadius(1000);
            background.setColor(randomAndroidColor);
            txtDetailPlaceHolder.setBackground(background);
            //imgDetailAvatar.setVisibility(View.INVISIBLE);

        } else
            Glide.with(imgDetailAvatar.getContext()).load(person.getAvatar()).apply(RequestOptions.circleCropTransform()).into(imgDetailAvatar);
            //txtDetailPlaceHolder.setVisibility(View.INVISIBLE);
//
            //imgDetailAvatar.setImageBitmap(BitmapFactory.decodeFile(person.getAvatar()));
        }

        imgSMS.setOnClickListener(Click_SMS);
        imgCall.setOnClickListener(Click_Call);
        imgEmail.setOnClickListener(Click_Email);
    }

    public String getShortName(String name) {
        String[] strings = name.split(" ");//no i18n
        String shortName;
        if (strings.length == 1) {
            shortName = strings[0].substring(0, 1);
        } else {
            shortName = strings[0].substring(0, 1);
        }
        return shortName.toUpperCase();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 20) {
                if (resultCode == 200) {
                    Person select = (Person) data.getSerializableExtra("EDIT");
                    SQLitePerson personsql = new SQLitePerson(getApplicationContext());
                    txtDetailName.setText(select.getName());
                    txtDetailPhone.setText(select.getPhone());
                    txtDetailMail.setText(select.getEmail());
                    imgDetailAvatar.setImageBitmap(BitmapFactory.decodeFile(select.getAvatar()));
                    if (select.getAvatar() == null) {
                        txtDetailPlaceHolder.setText(getShortName(select.getName().toString()));
                        int[] androidColors = getResources().getIntArray(R.array.androidcolors);
                        int randomAndroidColor = androidColors[new Random().nextInt(androidColors.length)];
                        GradientDrawable background = new GradientDrawable();
                        background.setCornerRadius(1000);
                        background.setColor(randomAndroidColor);
                        txtDetailPlaceHolder.setBackground(background);

                    } else {
                        Glide.with(imgDetailAvatar.getContext()).load(select.getAvatar()).apply(RequestOptions.circleCropTransform()).into(imgDetailAvatar);
                        txtDetailPlaceHolder.setVisibility(View.INVISIBLE);
//
                        //imgDetailAvatar.setImageBitmap(BitmapFactory.decodeFile(select.getAvatar()));
                    }
                    personsql.updatePerson(select);
                    /*Intent in = new Intent(DetailActivity.this, MainActivity.class);
                    in.putExtra("DETAIL", select);
                    startActivityForResult(in, 300);*/
                } /*else if (resultCode == 300){
                    int i = data.getExtras().getInt("DELETE");
                    Intent intent = new Intent(DetailActivity.this, MainActivity.class);
                    intent.putExtra("DELETEITEM", i);
                    startActivityForResult(intent, 20);
                    setResult(300, intent);

                }*/
            }
        }
        private View.OnClickListener Click_SMS = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sendIntent = new Intent(Intent.ACTION_SENDTO,
                        Uri.fromParts("smsto", person.getPhone(), null));
                sendIntent.putExtra("sms_body", "I miss you so much");
                view.getContext().startActivity(sendIntent);
            }
        };
        private View.OnClickListener Click_Call = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_CALL,
                        Uri.parse("tel:" + person.getPhone()));

                if (ActivityCompat.checkSelfPermission(view.getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                view.getContext().startActivity(intent);
            }
        };
        private View.OnClickListener Click_Email = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent email = new Intent(Intent.ACTION_SEND);
                email.putExtra(Intent.EXTRA_EMAIL, new String[]{person.getEmail()});
                email.putExtra(Intent.EXTRA_SUBJECT, "");
                email.putExtra(Intent.EXTRA_TEXT, "");
                email.setType("message/rfc822");
                view.getContext().startActivity(Intent.createChooser(email, "Chọn cách thức gửi mail"));
            }
        };

    }

