package com.example.morejump.contactapp;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

public class AddActivity extends AppCompatActivity {
    EditText edtAddFirstName, edtAddLastName, edtAddPhone, edtAddMail;
    Button btnSaveAdd;
    ImageView imgChooseImage;
    private int RESULT_LOAD_IMAGE = 1;
   private String picturePath;
    Person person;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        initView();
    }

    public void initView(){
        edtAddFirstName = (EditText) findViewById(R.id.edtAddFirstName);
        edtAddLastName = (EditText) findViewById(R.id.edtAddLastName);
        edtAddPhone = (EditText) findViewById(R.id.edtAddPhone);
        edtAddMail = (EditText) findViewById(R.id.edtAddMail);
        btnSaveAdd = (Button) findViewById(R.id.btnSaveAdd);
        imgChooseImage = (ImageView) findViewById(R.id.imgChooseImage);

        person = new Person();

        btnSaveAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                person.setName(edtAddFirstName.getText().toString() + " " + edtAddLastName.getText().toString().trim());
                person.setEmail(edtAddMail.getText().toString().trim());
                person.setPhone(edtAddPhone.getText().toString().trim());
                person.setId(0);
//
//
//                Log.d("========",picturePath);
//
//                Person person = new Person(0, edtAddFirstName.getText().toString() + " " + edtAddLastName.getText().toString(),
//                        edtAddPhone.getText().toString(), edtAddMail.getText().toString(), picturePath);
                Intent intent = new Intent(AddActivity.this, MainActivity.class);
                intent.putExtra("ADD", person);
                setResult(100, intent);
                finish();
            }
        });
        //Log.d("12", picturePath);
        imgChooseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, RESULT_LOAD_IMAGE);
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Log.d("", "onActivityResult: ");
            Uri selectedImage = data.getData();
            //imgChooseImage.setImageURI(selectedImage);
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();
            Glide.with(imgChooseImage.getContext()).load(picturePath).apply(RequestOptions.circleCropTransform()).into(imgChooseImage);
//
            //imgChooseImage.setImageBitmap(BitmapFactory.decodeFile(picturePath));
            person.setAvatar(picturePath);
        }
    }
}
