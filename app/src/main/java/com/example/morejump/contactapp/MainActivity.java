package com.example.morejump.contactapp;

import android.app.ActionBar;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    ListView lvData;
    PersonAdapter adapter;
    ArrayList<Person> listData;
    EditText edtSearch;
    Boolean checked = false;
    Map<String,Integer> mapIndex;
    private Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //Không hiện tiêu đề
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //Hiện nút back
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AddActivity.class);
                startActivityForResult(intent, 10);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        createDB();
        getListPS();
        initView();



    }

    @Override
    public void onBackPressed() {
        this.menu.clear();
        getMenuInflater().inflate(R.menu.main, this.menu);
        setLvItemClickIsShowDetailMode();
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
//            super.onBackPressed();
//        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        this.menu = menu;
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {
            case R.id.action_select:
                setLvItemClickIsSelectMode();
                break;

            case R.id.action_selectAll:
                setLvItemClickIsAllSelectMode();
                break;

            case R.id.action_settings: {

            }
            case R.id.action_delete:
                SQLitePerson person  = new SQLitePerson(getApplicationContext());
                for (Person ps : adapter.getListData()){
                    if (ps.isChecked()){
                        person.deletePerson(ps.getId());
                        adapter.notifyDataSetChanged();
                        getListPS();
                        initView();
                    }
                }
                break;
            case R.id.action_share:
                int count = 0;
                int index = 0;
                for (int i=0 ; i<adapter.getListData().size(); i++){
                    if (adapter.getItem(i).isChecked()){
                        count++;
                        index = i;
                    }
                }
                if (count == 1){
                    Intent sendIntent = new Intent(Intent.ACTION_SENDTO,
                            Uri.fromParts("smsto", "", null));
                    sendIntent.putExtra("sms_body", adapter.getItem(index).getName() + "\n" +
                    adapter.getItem(index).getPhone() + "\n" + adapter.getItem(index).getEmail());
                    startActivity(sendIntent);

                } else {
                    Toast.makeText(MainActivity.this, "Select one contact to share anyone!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_listAllContact) {
            // Handle the camera action
        } else if (id == R.id.nav_listContact) {

        } else if (id == R.id.nav_friend) {

        } else if (id == R.id.nav_createLabel) {

        } else if (id == R.id.nav_setting) {

        } else if (id == R.id.nav_help) {

        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

//    private void getData() {
//        listData = new ArrayList<>();
//        listData.add(new Person(0, "Quynh Anh"));
//        listData.add(new Person(1, "Anh Anh"));
//        listData.add(new Person(2, "Nam Anh"));
//        listData.add(new Person(3, "Van Anh"));
//        listData.add(new Person(4, "Tuan Anh"));
//        listData.add(new Person(5, "Long Anh"));
//
//    }


    @Override
    protected void onResume() {

        super.onResume();
        createDB();
        getListPS();
        initView();
    }


    private void initView() {
        //sortArrayList(listData);
        adapter = new PersonAdapter(listData, MainActivity.this);
        lvData = (ListView) findViewById(R.id.lvData);
        edtSearch = findViewById(R.id.edtSearch);
        lvData.setAdapter(adapter);
        for (Person ps : adapter.getListData()) {
            ps.setChecked(checked);
        }
        adapter.notifyDataSetChanged();
        /*lvData.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                intent.putExtra("DETAIL", adapter.getItem(i));
                startActivity(intent);
            }
        });*/

        setLvItemClickIsShowDetailMode();
        //ArrayAdapter<PersonAdapter> psAdapter = new ArrayAdapter<PersonAdapter>(this, R.layout.side_index_item);

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                findPerson(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    private AdapterView.OnItemClickListener Detail_Contact = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            Intent intent = new Intent(MainActivity.this, DetailActivity.class);
            intent.putExtra("DETAIL", adapter.getItem(i));
            startActivity(intent);
        }
    };


    private void createDB() {
// khởi tạo database
        SQLiteDataController sql = new SQLiteDataController(this);
        try {
            sql.isCreatedDatabase();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getListPS() {
        SQLitePerson person = new SQLitePerson(getApplicationContext());
        listData = new ArrayList<>();
        listData = person.getListPerson();
        Collections.sort(listData, new Comparator<Person>() {
            @Override
            public int compare(Person p1, Person p2) {
                return p1.getName().compareTo(p2.getName());
            }
        });
    }

   /* private void sortArrayList(ArrayList<Person> listPerson) {
        try {

            Collections.sort(listData, new Comparator<Person>() {
                @Override
                public int compare(Person p1, Person p2) {
                    return p1.getName().compareTo(p2.getName());
                }
            });
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }*/


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 10:
                if (data != null) {
                    Person person;
                    SQLitePerson personsql = new SQLitePerson(getApplicationContext());
                    try {

                        person = (Person) data.getSerializableExtra("ADD");
                        if (resultCode == 100) {
                            person.setId(adapter.getCount() + 1);
                            adapter.getListData().add(person);
                            adapter.notifyDataSetChanged();
                            personsql.insertPerson(person);
                            Toast.makeText(MainActivity.this, "ADD Complete !", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(MainActivity.this, "ADD Failed, Try again !", Toast.LENGTH_SHORT).show();

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

        }


    }

    public void findPerson(CharSequence key) {
        ArrayList<Person> listSearch = new ArrayList<>();
        for (Person ps : listData) {
            if (ps.getName().toLowerCase().contains(key)) {
                listSearch.add(ps);
            } else if (ps.getPhone().contains(key)){
                listSearch.add(ps);
            } else if (ps.getEmail().contains(key)){
                listSearch.add(ps);
            }
        }
        PersonAdapter searchResult = new PersonAdapter(listSearch, MainActivity.this);
        lvData.setAdapter(searchResult);
    }


    public void setLvItemClickIsAllSelectMode() {
        for (Person ps : adapter.getListData()) {
            ps.setChecked(true);
            adapter.notifyDataSetChanged();
            lvData.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    adapter.getItem(i).setChecked(!adapter.getItem(i).isChecked());
                    adapter.notifyDataSetChanged();
                }
            });
            menu.clear();
            getMenuInflater().inflate(R.menu.menu_select_item, this.menu);
        }
    }
    public void setLvItemClickIsSelectMode(){
        lvData.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                adapter.getItem(i).setChecked(!adapter.getItem(i).isChecked());
                adapter.notifyDataSetChanged();
            }
        });
        menu.clear();
        getMenuInflater().inflate(R.menu.menu_select_item, this.menu);
    }

    public void setLvItemClickIsShowDetailMode(){
        for (Person ps : adapter.getListData()) {
            ps.setChecked(false);
            adapter.notifyDataSetChanged();
            lvData.setOnItemClickListener(Detail_Contact);
        }
    }


}
