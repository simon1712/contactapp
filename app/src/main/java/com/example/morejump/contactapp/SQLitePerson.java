package com.example.morejump.contactapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by morejump on 09/11/2017.
 */

public class SQLitePerson extends SQLiteDataController {

    public SQLitePerson(Context context) {
        super(context);
    }

    public ArrayList<Person> getListPerson(){
        ArrayList<Person> listPerson = new ArrayList<>();
        try {
            openDataBase();
            Cursor cs = database.rawQuery("Select id, name, phone, email, avatar from Person", null);
            Person person;
            while (cs.moveToNext()) {
                person = new Person(cs.getInt(0), cs.getString(1), cs.getString(2), cs.getString(3), cs.getString(4));
                listPerson.add(person);
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            close();
        }
    return listPerson;
    }

    public boolean insertPerson(Person ps) {
        boolean result = false;
        try {

            openDataBase();
            ContentValues values = new ContentValues();
            values.put("id", ps.getId());
            values.put("name", ps.getName());
            values.put("phone", ps.getPhone());
            values.put("email", ps.getEmail());
            values.put("avatar", ps.getAvatar());
            long rs = database.insert("Person", null, values);
            if (rs > 0) {
                result = true;
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            close();
        }
        return result;
    }


    public boolean updatePerson(Person ps) {
        boolean result = false;
        try {

            openDataBase();
            ContentValues values = new ContentValues();
            values.put("id", ps.getId());
            values.put("name", ps.getName());
            values.put("phone", ps.getPhone());
            values.put("email", ps.getEmail());
            values.put("avatar", ps.getAvatar());
            int rs = database.update("Person", values, "id=" + ps.getId(), null);
            if (rs > 0) {
                result = true;
            }
        }
        catch (SQLiteException e) {
            e.printStackTrace();
        }
        finally
        {
            close();
        }
        return result;
    }

    public boolean deletePerson(int id) {
        boolean result = false;
        try {

            openDataBase();
            //
            int rs = database.delete("Person", "id=" + id, null);
            if (rs > 0) {
                result = true;
            }
        }catch (SQLiteException e) {
            e.printStackTrace();
        }  finally {
            close();
        }
        return result;
    }
}

