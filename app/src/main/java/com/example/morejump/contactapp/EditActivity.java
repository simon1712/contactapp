package com.example.morejump.contactapp;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

public class EditActivity extends AppCompatActivity {
    EditText edtEditName, edtEditPhone, edtEditMail;
    ImageView imgEditAvatar;
    Button btnSaveEdit, btnDelete;
    String picturePath = null;
    private int RESULT_LOAD_IMAGE = 1;
    Person person;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        initView();
        getData();
    }


    public void initView(){
        edtEditName = (EditText) findViewById(R.id.edtEditName);
        edtEditPhone = (EditText) findViewById(R.id.edtEditPhone);
        edtEditMail = (EditText) findViewById(R.id.edtEditMail);
        imgEditAvatar = (ImageView) findViewById(R.id.imgEditAvatar);
        btnSaveEdit = (Button) findViewById(R.id.btnSaveEdit);
        btnDelete = (Button) findViewById(R.id.btnDelete);
        imgEditAvatar.setOnClickListener(Change_Avatar);
        btnSaveEdit.setOnClickListener(Save_Item);
        btnDelete.setOnClickListener(Delete_Item);
    }

    private void getData(){
        if (getIntent().getExtras() != null){
            Log.d("1234", "getData: ");
            person = (Person) getIntent().getSerializableExtra("EDIT");
            int a = person.getId();
            edtEditName.setText(person.getName());
            edtEditPhone.setText(person.getPhone());
            edtEditMail.setText(person.getEmail());
            if (person.getAvatar() != null){
                Glide.with(imgEditAvatar.getContext()).load(person.getAvatar()).apply(RequestOptions.circleCropTransform()).into(imgEditAvatar);
            }
            //imgEditAvatar.setImageBitmap(BitmapFactory.decodeFile(person.getAvatar()));
        }
    }


    View.OnClickListener Change_Avatar = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, RESULT_LOAD_IMAGE);
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            //imgChooseImage.setImageURI(selectedImage);
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();
            Glide.with(imgEditAvatar.getContext()).load(picturePath).apply(RequestOptions.circleCropTransform()).into(imgEditAvatar);
            //placeholder.setVisibility(View.INVISIBLE);
            //imgEditAvatar.setImageBitmap(BitmapFactory.decodeFile(picturePath));
            person.setAvatar(picturePath);
        }
    }


    View.OnClickListener Save_Item = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int b = person.getId();
            person.setName(edtEditName.getText().toString().trim());
            person.setEmail(edtEditMail.getText().toString().trim());
            person.setPhone(edtEditPhone.getText().toString().trim());
           // Glide.with(imgEditAvatar.getContext()).load(person.getAvatar()).apply(RequestOptions.circleCropTransform()).into(imgEditAvatar);


            Log.d("thic", person.getId() +person.getName() + person.getPhone() + person.getEmail() + person.getAvatar());
//
//
//                Log.d("========",picturePath);
//
//                Person person = new Person(0, edtAddFirstName.getText().toString() + " " + edtAddLastName.getText().toString(),
//                        edtAddPhone.getText().toString(), edtAddMail.getText().toString(), picturePath);
            Intent intent = new Intent(EditActivity.this, DetailActivity.class);
            intent.putExtra("EDIT", person);
            setResult(200, intent);
            finish();
        }
    };

    View.OnClickListener Delete_Item = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            SQLitePerson ps  = new SQLitePerson(view.getContext());
            ps.deletePerson(person.getId());
            Intent intent = new Intent(EditActivity.this, MainActivity.class);
            intent.putExtra("DELETE", person.getId());
            Toast.makeText(EditActivity.this, "Delete Complete", Toast.LENGTH_SHORT).show();
            startActivity(intent);
        }
    };
}
